﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace sp_hw5
{
    public class AccountNoSync
    {
        public int Balance { get; set; }


        public void AddCash(object cash)
        {
            
            Thread.Sleep(150);
            Balance += (int)cash;
            Console.WriteLine($"Thread: {Thread.CurrentThread.ManagedThreadId} startted to work AddCash \nBalance: {Balance}");

        }

        public void SubstractCash(object cash)
        {
            Thread.Sleep(150);
            Balance -= (int)cash;
            Console.WriteLine($"Thread: {Thread.CurrentThread.ManagedThreadId} startted to work SubstractCash \nBalance: {Balance}");

        }

        public int GetBalance()
        {
            return Balance;
        }

        public void ShowBalance()
        {
            Console.WriteLine($"Current Balance is: {Balance}");
        }
    }
}
