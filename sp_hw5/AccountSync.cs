﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace sp_hw5
{
    public class AccountSync
    {
        public int Balance { get; set; } = 0;
        private object lockObject = new object();

        public void AddCash(object cash)
        {
            lock (lockObject)
            {
                Thread.Sleep(150);
                Console.WriteLine($"Thread: {Thread.CurrentThread.ManagedThreadId} startted to work");
                Balance += (int)cash;
                Console.WriteLine($"Balance: {Balance}");
            }
        }

        public void SubstractCash(object cash)
        {
            
            lock (lockObject)
            {
                Thread.Sleep(150);
                Console.WriteLine($"Thread: {Thread.CurrentThread.ManagedThreadId} startted to work");
                Balance -= (int)cash;
                Console.WriteLine($"Balance: {Balance}");
            }
        }

        public int GetBalance()
        {
            return Balance;
        }

        public void ShowBalance()
        {
            Console.WriteLine($"Current Balance is: {Balance}");
        }
    }
}
