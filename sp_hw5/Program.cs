﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace sp_hw5
{
    class Program
    {
        static void Main(string[] args)
        {
            var threads = new Thread[40];

            var accountNoSync = new AccountNoSync();
            var accountSync = new AccountSync();
            accountNoSync.Balance = 0;

            Console.WriteLine("\nAccount no Sync started to work:");
            for (int i = 0; i < 20; i++)
            {
                threads[i] = new Thread(accountNoSync.SubstractCash);
            }

            for (int i = 20; i <threads.Length; i++)
            {
                threads[i] = new Thread(accountNoSync.AddCash);
            }

            foreach (var thread in threads)
            {
                thread.Start(1000);
       
            }

            Thread.Sleep(2000);

            Console.WriteLine("\nAccount Sync started to work:");
            for (int i = 0; i < 20; i++)
            {
                threads[i] = new Thread(accountSync.SubstractCash);
            }

            for (int i = 20; i < threads.Length; i++)
            {
                threads[i] = new Thread(accountSync.AddCash);
            }

            foreach (var thread in threads)
            {
                thread.Start(1000);
            }

            Thread.Sleep(2000);

            Console.ReadLine();
        }
    }
}
